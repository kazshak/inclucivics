import React from "react"
import InfoSection from "../InfoSection/InfoSection"

const Stat = ({ stat, id, nextId, prevId, onClick }) => {
    return (
    <div>
        <InfoSection id={id} nextId={nextId} prevId={prevId} onClick={onClick}>
            <div>{stat.text}</div>
        </InfoSection>
        </div>
    )
}

export default Stat