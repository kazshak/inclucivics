import React from "react"
import PrevButton from "./PrevButton"
import NextButton from "./NextButton"

const SectionNav = ({ nextId, prevId, onClick }) => {
    return <div>
        {nextId && <NextButton nextId={nextId} onClick={onClick} />}
        {prevId && <PrevButton prevId={prevId} onClick={onClick} />}
    </div>
}

export default SectionNav