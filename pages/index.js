import Head from "next/head";
import Home from "../components/Home";
import styles from "../styles/Home.module.css";
import Problem from "../components/Problem/Problem";
import Stat from "../components/Stat/Stat";
import problems from "../data/problems.json"
import stats from "../data/stats.json"


export default function Index() {   
  const handleNavigation = (event) => {
    event.preventDefault();
    const element = document.querySelector(event.target.hash);
    element.scrollIntoView({ behavior: "smooth", block: "start" });
  };

  const itemCount = problems.length + stats.length
  let currentItem = 0

  return (
    <div className={styles.container}>
      <Head>
        <title>Inclucivics</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Home />
        <div id="problems">
          {
            problems.map((p, idx) => {
              currentItem += 1
              const prevId = idx === 0 ? null : `problem-${idx - 1}`
              const nextId = idx + 1 === problems.length ? `stats-0` : `problem-${idx + 1}`
              return (
                <Problem key={idx} problem={p} id={`problem-${idx}`} nextId={nextId} prevId={prevId} onClick={handleNavigation}/>
              )
            })
          }
          {
            stats.map((s, idx) => {
              currentItem += 1
              const prevId = idx === 0 ? `problem-${problems.length -1}` : `stats-${idx - 1}`
              const nextId = idx + 1 === stats.length ? `solutions-0` : `stats-${idx + 1}`
              const isLastItem = currentItem === itemCount ? true : false
              return (
                <Stat key={idx} stat={s} id={`stats-${idx}`} nextId={isLastItem ? null : nextId} prevId={prevId} onClick={handleNavigation}/>
              )
            })
           } 
        </div>
      </main>
      <footer className={styles.footer}></footer>
    </div>
  );
}


// figure out IF an element actually exists BEFORE we try to link to it (meanig in nextid -> seciontNav (conditionally render it))